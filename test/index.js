describe("Testing Put", function() {

    beforeEach(module("eTodo"));

    var MainCtrl, scope;

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();
        MainCtrl = $controller("MainCtrl", {
            $scope: scope
        });

        var $injector = angular.injector(['ng', 'pouchdb']);
        var pouchDB = $injector.get('pouchDB');
        db = pouchDB('db');
    }));

    it('should call put mehtod in SCOPE.create', function(done) {
        scope.task = {
            _id: new Date().toISOString(),
            title: 'Test my task',
            description: 'Testing my new task',
            completed: false
        }

        scope.create();
        db.get(scope.task._id, function(error, results) {
            expect(results._id).toBe(scope.task._id);
            done()
        })
    });

});
