"use strict";

const app = angular.module('eTodo', ['pouchdb']);

app.controller('MainCtrl', function($scope, pouchDB) {

    var db = pouchDB('db');

    $scope.tasks = []
    $scope.task = {
        completed: false
    }

    $scope.getAll = function() {
        db.allDocs({ include_docs: true, descending: true }, function(error, results) {
            $scope.tasks = results.rows;
            $scope.$apply()
        });
    }

    // create task
    $scope.create = function() {
        // Update _id
        $scope.task['_id'] = new Date().toISOString();

        db.put($scope.task, function(error, results) {
            if (!error) {
                $scope.task = {};
                $scope.getAll();

                angular.element('#todoModal').modal('hide');
            }
        });
    }

    // delete task
    $scope.delete = function(event) {
        let element = angular.element(event.target),
            data = (element.data());
        db.remove(data.docId, data.docRev, function(error, results) {
            if (!error) {
                $scope.getAll();
            }
        });
    }

    // mark task as completed.
    $scope.done = function(event) {
        let element = angular.element(event.target),
            data = (element.data());
        db.get(data.docId, function(error, result) {
            if (error) return;
            result['completed'] = true;
            db.put(result, function(error, results) {
                if (!error) {
                    $scope.getAll();
                }
            });
        });
    }
});
